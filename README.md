# Utility - Filter - Basic2List

Utility is a generic framework for command-line utility programs.
Copy template.py to make new programs, or see Filter.py for a complete
example app.

### Filter

```
Usage: Filter.py [options]
A grep/sed/awk-like filter built on Utility.
	Use filename - to read stdin

	Examples:
		Filter.py -s "(?i)copyright" license.txt
			# prints all lines mentioning copyright, ignoring case.
			# Use `Filter.py --rehelp` to look up regular expression usage!

		Filter.py -E example-center.py license.txt
			# Runs all lines through example-center.py script, which centers
			# the text horizontally.

		Filter.py -b "self.data.total = 0" \
			-s "^(-?[0-9.]*)\s+(.*)$" -r "\1" \
			-e "self.data.total += float(line)" \
			-a "print('T:', self.data.total, ', AVG:', self.data.total / max(1, self.matched) )" \
			example-data.txt
			# -b (--before) script creates a running total.
			# -s (--search) and -r (--replace) match the first column of numbers.
			# -e (--exec) adds the number column to the total.
			# -a (--after) script prints the total and average of all lines.
			#	(I used max() to make sure we don't divide by 0)
			# Result should be T: 15.5 , AVG: 5.166666666666667

	Filter has these instance fields, in addition to Utility's:
		* self.matched
			Count of all lines which match the -s pattern.
		* self.filename
			Current filename.
		* self.linenum
			Current line number.
		Type `Utility.py -h` for more.


Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -H, --rehelp          show regular expression help and exit
  -l RANGE, --lines=RANGE
                        operate on range #-# of lines, inclusive
  -s SEARCH, --search=SEARCH
                        search for pattern
  -r REPLACE, --replace=REPLACE
                        replace search with pattern
  -b SCRIPT, --before=SCRIPT
                        exec Python statements before running
  -B SCRIPTFILE, --beforefile=SCRIPTFILE
                        exec Python script file before running
  -e SCRIPT, --exec=SCRIPT
                        for each match, exec Python statements with variable
                        'line'
  -E SCRIPTFILE, --execfile=SCRIPTFILE
                        for each match, exec Python script file with variable
                        'line'
  -a SCRIPT, --after=SCRIPT
                        exec Python statements after running
  -A SCRIPTFILE, --afterfile=SCRIPTFILE
                        exec Python script file after running
  -c, --count           display count of matching lines at end
  -n, --number          show 'filename:linenum:' before lines
  -p, --printall        print (but don't replace/exec!) all lines, even if
                        they don't match SEARCH
```


### Basic2List

Process nice, formatted BASIC source into tightly-packed lines.

- Takes input file .BAS, produces .LST (Atari BASIC text source file).
- Leading/trailing whitespace, lines starting #, or anywhere inline // comments, are removed.
- `\\xFF` inserts byte with hex value FF, currently no other escapes allowed,
- and do not use `\\` in source!
- All lines NOT starting with a number are joined to the previous numbered line with a colon.
- Warns if lines exceed 120 chars.
- Optionally generate either --atascii or --dos files
