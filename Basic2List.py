#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys
import Utility

class Basic2List(Utility.Utility):
	"""Process nice, formatted BASIC source into tightly-packed lines.
	Takes input file .BAS, produces .LST
	Leading/trailing whitespace, lines starting #, or anywhere inline // comments, are removed.
	\\xFF inserts byte with hex value FF, currently no other escapes allowed,
	and do not use \\ in source!
	All lines NOT starting with a number are joined to the previous numbered
	line with a colon.
	Warns if lines exceed 120 chars.
	"""

	def initOptions(self):
		self.optparse.add_option("-a", "--atascii",
			action="store_true", dest="atascii", default=False,
			help="emit ATASCII 155 newlines")
		self.optparse.add_option("-d", "--dos",
			action="store_true", dest="dos", default=False,
			help="emit DOS (CP/M) CR/LF newlines")

	def initFinal(self):
		if len(self.args) == 0:
			self.optparse.print_help()
			sys.exit(1)
		self.linesout = 0

	def run(self):
		self.nl = bytes([10])
		if self.options.atascii: self.nl = bytes([155])
		if self.options.dos: self.nl = bytes([13, 10])
		for filename in self.args:
			p, e = os.path.splitext(filename)
			if e not in (".bas", ".BAS"):
				raise Exception("Expected .BAS, but got "+filename)
			outfile = p+".LST"
			print("%s => %s" % (filename, outfile))
			try:
				self.inf = open(filename, "rb")
				self.outf = open(outfile, "wb")
				self.runFile()
			except Exception as e:
				print(e, file=sys.stdout)
				if self.inf:
					self.inf.close()
					self.inf = None
				if self.outf:
					self.outf.close()
					self.outf = None

	def runFile(self):
		self.buf = b''
		linenum = 0
		for line in self.inf:
			linenum += 1
			line = self.processComments(line)
			line = self.processChars(line)
		self.emitBuffer()
		print("Wrote %d lines" % (self.linesout,))

	def processComments(self, line):
		i = line.find(b'//')
		if i >= 0: line = line[0:i]
		# matches /[ \t]+#/
		for i in range(len(line)):
			if i == b' ' || i == b'\t': pass
			elif i == b'#':
				line = b''
				break
			else: break
		return line

	# TODO: pull out line number parsing
	# TODO: remember all line numbers, error on duplicate
	def processChars(self, line):
		state = '1'
		num = b''
		text = b''
		i = -1
		while i < len(line):
			i += 1
			c = line[i:i+1]
			if state == '1':
				if c.isdigit(): num += c
				elif not num and c.isspace(): pass
				elif num and c.isspace(): state = 't'
				else: i -= 1; state = 't'
			elif state == 't':
				if c == b'\\':
					if line[i+1:i+2] == b'x':
						cx = line[i+2:i+4]
						text += bytes([ int(cx,16) ])
						i += 3
					else: raise Exception("Unknown escape at %d in %s" % (i, line))
				else: text += c
		text = text.strip()
		if num:
			self.emitBuffer()
			self.buf = num + b' ' + text
		elif text:
			self.buf += b':'+text
		return b''	# has consumed line

	def emitBuffer(self):
		if self.buf:
			self.outf.write(self.buf)
			self.outf.write(self.nl)
			self.linesout += 1
			if len(self.buf) >= 120:
				print("Warning: Line %d chars\n%s" % (len(self.buf), self.buf,), file=sys.stderr)

if __name__ == '__main__':
	Utility.main(Basic2List())
