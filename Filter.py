#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2009,2018 by Mark Hughes.	 All Rights Reserved.
#
# Redistribution and use in source and binary forms, with or
# without modification, are permitted provided that the
# following conditions are met:
#
# * Redistributions of source code must retain the above
#	copyright notice, this list of conditions and the following
#	disclaimer.
# * Redistributions in binary form must reproduce the
#	above copyright notice, this list of conditions and
#	the following disclaimer in the documentation and/or
#	other materials provided with the distribution.
# * Neither the name of the software owner nor the name
#	of any contributors may be used to endorse or promote
#	products derived from this software without specific
#	prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
# CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import Utility, re, sys

class Filter(Utility.Utility):
	"""A grep/sed/awk-like filter built on Utility.
	Use filename - to read stdin

	Examples:
		Filter.py -s "(?i)copyright" license.txt
			# prints all lines mentioning copyright, ignoring case.
			# Use `Filter.py --rehelp` to look up regular expression usage!

		Filter.py -E example-center.py license.txt
			# Runs all lines through example-center.py script, which centers
			# the text horizontally.

		Filter.py -b "self.data.total = 0" \\
			-s "^(-?[0-9.]*)\s+(.*)$" -r "\\1" \\
			-e "self.data.total += float(line)" \\
			-a "print('T:', self.data.total, ', AVG:', self.data.total / max(1, self.matched) )" \\
			example-data.txt
			# -b (--before) script creates a running total.
			# -s (--search) and -r (--replace) match the first column of numbers.
			# -e (--exec) adds the number column to the total.
			# -a (--after) script prints the total and average of all lines.
			#	(I used max() to make sure we don't divide by 0)
			# Result should be T: 15.5 , AVG: 5.166666666666667

	Filter has these instance fields, in addition to Utility's:
		* self.matched
			Count of all lines which match the -s pattern.
		* self.filename
			Current filename.
		* self.linenum
			Current line number.
		Type `Utility.py -h` for more.
	"""

	def initOptions(self):
		self.optparse.add_option("-H", "--rehelp",
				action="store_true", dest="rehelp", default=False,
				help="show regular expression help and exit")

		self.optparse.add_option("-l", "--lines",
				dest="range", default=None,
				help="operate on range #-# of lines, inclusive")
		self.optparse.add_option("-s", "--search",
				dest="search", default=r"^(.*)$",
				help="search for pattern", metavar="SEARCH")
		self.optparse.add_option("-r", "--replace",
				dest="replace",
				help="replace search with pattern", metavar="REPLACE")

		self.optparse.add_option("-b", "--before", dest="before",
				help="exec Python statements before running", metavar="SCRIPT")
		self.optparse.add_option("-B", "--beforefile", dest="beforefile",
				help="exec Python script file before running", metavar="SCRIPTFILE")
		self.optparse.add_option("-e", "--exec", dest="eval",
				help="for each match, exec Python statements with variable 'line'", metavar="SCRIPT")
		self.optparse.add_option("-E", "--execfile", dest="evalfile",
				help="for each match, exec Python script file with variable 'line'", metavar="SCRIPTFILE")
		self.optparse.add_option("-a", "--after", dest="after",
				help="exec Python statements after running", metavar="SCRIPT")
		self.optparse.add_option("-A", "--afterfile", dest="afterfile",
				help="exec Python script file after running", metavar="SCRIPTFILE")

		self.optparse.add_option("-c", "--count",
				action="store_true", dest="count", default=False,
				help="display count of matching lines at end")
		self.optparse.add_option("-n", "--number",
				action="store_true", dest="number", default=False,
				help="show 'filename:linenum:' before lines")
		self.optparse.add_option("-p", "--printall",
				action="store_true", dest="printall", default=False,
				help="print (but don't replace/exec!) all lines, even if they don't match SEARCH")

	def initFinal(self):
		if len(sys.argv) <= 1:
			self.optparse.print_help()
			sys.exit(1)
		if self.options.rehelp:
			help(re)
			sys.exit(1)
		if self.options.search is not None:
			self.searchpat = re.compile(self.options.search)
		else:
			self.optparse.error("Missing search!") # can't happen, right?
		if self.options.range:
			words = self.options.range.split("-")
			if len(words) != 2: self.optparse.error("Invalid range")
			if words[0]: self.lineStart = int(words[0])
			else: self.lineStart = 0
			if words[1]: self.lineEnd = int(words[1])
			else: self.lineEnd = sys.maxsize
		else:
			self.lineStart = 0
			self.lineEnd = sys.maxsize
		self.matched = 0
		if self.options.evalfile:
			if self.options.eval:
				self.optparse.error("Options -e and -f are mutually exclusive")
			self.options.eval = open(self.options.evalfile).read()
		#print(self.searchpat)

	def processLine(self, line):
		line = line.rstrip()
		match = self.searchpat.search(line)
		if match:
			self.matched += 1
			if self.options.replace:
				line = self.searchpat.sub(self.options.replace, line)
			self.processMatch(self.formatLine(line))
		elif self.options.printall:
			print(self.formatLine(line))

	def processMatch(self, line):
		if self.options.eval:
			exec(self.options.eval)
		else:
			print(line)

	def formatLine(self, line):
		if self.options.number:
			return u"%s:%d:%s" % (self.filename, self.linenum, line)
		else:
			return line

	def run(self):
		if self.options.before:
			exec(self.options.before)
		for f in self.args:
			self.filename = f
			self.linenum = 0
			if f == "-":
				input = sys.stdin
			else:
				input = open(f)
			for line in input:
				self.linenum += 1
				if self.linenum >= self.lineStart:
					self.processLine(line)
				if self.linenum >= self.lineEnd:
					break
			if f != "-":
				input.close()
		if self.options.after:
			exec(self.options.after)
		if self.options.count:
			print(self.matched)
		if self.matched:
			return 0
		else:
			return 1

	def version(self):
		return "1.2"

if __name__ == '__main__':
	Utility.main(Filter())
