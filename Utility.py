#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2006,2018 by Mark Hughes.  All Rights Reserved.
#
# Redistribution and use in source and binary forms, with or
# without modification, are permitted provided that the
# following conditions are met:
#
# * Redistributions of source code must retain the above
#	copyright notice, this list of conditions and the following
#	disclaimer.
# * Redistributions in binary form must reproduce the
#	above copyright notice, this list of conditions and
#	the following disclaimer in the documentation and/or
#	other materials provided with the distribution.
# * Neither the name of the software owner nor the name
#	of any contributors may be used to endorse or promote
#	products derived from this software without specific
#	prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
# CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os, sys
from optparse import OptionParser

class DataHolder(object):
	def __str__(self):
		return str(self.__dict__)

class Utility:
	"""Utility is a generic framework for command-line utility programs.
	Copy template.py to make new programs, or see Filter.py for a complete
	example app.

	Utility has these instance fields:
		* self.optparse
			The option parser, see Python module docs.
		* self.options
			Dictionary of options parsed from command line.
		* self.args
			List of filenames/non-option arguments.
		* self.env
			System environment.
		* self.data
			A blank object for storing temporary data in.
			Set fields on it as you like.
	"""

	def __init__(self):
		minPythonVersion = self.minPythonVersion()
		if sys.version < minPythonVersion:
			sys.stderr.write("Minimum version of "+__name__+" is "+
				minPythonVersion+", but you're using Python "+sys.version+"!\n")
			sys.exit(1)
		self.data = DataHolder()
		self.optparse = OptionParser(version="%prog "+self.version())
		self.optparse.usage += "\n" + self.__doc__
		self.initOptions()
		self.options, self.args = self.optparse.parse_args()
		self.env = os.environ
		self.initFinal()

	def initOptions(self):
		"""Initialize option parsing.  Replace this method with custom args.
			Examples:
			self.optparse.set_defaults(verbose=False)
			self.optparse.add_option("-f", "--file", dest="filename",
					help="write report to FILE", metavar="FILE")
			self.optparse.add_option("-q", "--quiet",
					action="store_false", dest="verbose", default=True,
					help="don't print status messages to stdout")
		"""
		pass

	def initFinal(self):
		"""Post-option initialization.
			Replace this method with whatever you need.
		"""
		pass

	def minPythonVersion(self):
		"""Returns a string containing the minimum Python version necessary to
		run this program.
		Default is "3.7".
		"""
		return "3.7"

	def run(self):
		"""Actual utility code goes here.  Return 0 for success, 1 for failure.
		"""
		return 0

	def version(self):
		"""Returns a string containing the version of the utility.
		Default is "1.0".
		"""
		return "1.0"

def main(util):
	"""Runs a utility class and exits with the return code.
		The end of every utility module should be:
		if __name__ == '__main__':
			Utility.main(MYUTIL()) # replace MYUTIL
	"""
	rc = util.run()
	if rc == None:
		rc = 0
	sys.exit(rc)

if __name__ == '__main__':
	main(Utility())
