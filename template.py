#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2006 by Mark Hughes.  All Rights Reserved.
#
# Redistribution and use in source and binary forms, with or
# without modification, are permitted provided that the
# following conditions are met:
#
# * Redistributions of source code must retain the above
#   copyright notice, this list of conditions and the following
#   disclaimer.
# * Redistributions in binary form must reproduce the
#   above copyright notice, this list of conditions and
#   the following disclaimer in the documentation and/or
#   other materials provided with the distribution.
# * Neither the name of the software owner nor the name
#   of any contributors may be used to endorse or promote
#   products derived from this software without specific
#   prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
# CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#FIXME: search and replace MYUTIL with the name of the utility.
#FIXME: then put your code in the FIXME blocks.

import Utility

class MYUTIL(Utility.Utility):
    def initOptions(self):
        """Initialize option parsing.  Replace this method with custom args.
            Examples:
            self.optparse.set_defaults(verbose=False)
            self.optparse.add_option("-f", "--file", dest="filename",
                    help="write report to FILE", metavar="FILE")
            self.optparse.add_option("-q", "--quiet",
                    action="store_false", dest="verbose", default=True,
                    help="don't print status messages to stdout")
        """
        #FIXME
        pass

    def initFinal(self):
        """Post-option initialization.  Replace this method with whatever you
            need.
        """
        #FIXME
        pass

    def run(self):
        """Actual utility code goes here.  Return 0 for success, 1 for failure.
        """
        print "MYUTIL options:", self.options
        print "MYUTIL args:", self.args
        #FIXME
        return 0

if __name__ == '__main__':
    Utility.main(MYUTIL())
